<?php

/*
 * Complete the 'countingValleys' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER steps
 *  2. STRING path
 */

function countingValleys($steps, $path) {
    // Write your code here
    $path = str_split($path);
    $elevation = 0;
    $valleys = 0;
    $inValley = false;
    foreach($path as $unit){
        // echo $elevation;
        if($unit == "U"){
            $elevation = $elevation+1;
        }else{
            $elevation = $elevation-1;
        }
        if($elevation < 0 && !$inValley){
            $valleys = $valleys + 1;
            $inValley = true;
        }else if($elevation == 0){
            $inValley=false;
        }
    }
    
    return $valleys;
}

$fptr = fopen(getenv("OUTPUT_PATH"), "w");

$steps = intval(trim(fgets(STDIN)));

$path = rtrim(fgets(STDIN), "\r\n");

$result = countingValleys($steps, $path);

fwrite($fptr, $result . "\n");

fclose($fptr);
