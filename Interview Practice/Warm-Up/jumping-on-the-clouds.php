<?php

/*
 * Complete the 'jumpingOnClouds' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts INTEGER_ARRAY c as parameter.
 */

function jumpingOnClouds($c) {
    $jumps = 0;
    for($i = 0; $i < count($c); $i++) {
       echo $i;
       if($i+1 < count($c) && $c[$i+2] == 0){
           $i=$i+1;
           $jumps++;
       }
       else if($i+1 < count($c) && $c[$i+1] == 0){
           $jumps++;
       }
    }
    
    return $jumps;
}

$fptr = fopen(getenv("OUTPUT_PATH"), "w");

$n = intval(trim(fgets(STDIN)));

$c_temp = rtrim(fgets(STDIN));

$c = array_map('intval', preg_split('/ /', $c_temp, -1, PREG_SPLIT_NO_EMPTY));

$result = jumpingOnClouds($c);

fwrite($fptr, $result . "\n");

fclose($fptr);
